
Übercart Sponsorhip module:
----------------------
Original author - Jérémy Chatard http://www.breek.fr
Author - Jérémy Chatard jchatard at breek dot fr
Requires - Drupal 5 & Übercart 1.0
License - GPL (see LICENSE)


Overview:
--------
Übercart Sponsorship let your customers invite friends to become member of your store, allowing them to get a discount.

Installation:
------------
1. Copy the uc_sponsorship directory to sites/all/modules/ubercart/contrib or sites/example.com/modules/ubercart/contrib directory.
2. Go to "Administer" -> "Site building" -> "modules" and enable the module.
3. Check the "sponsor friends" -> "Access control" page to enable use of this module to different roles.
4. Set up your discount amount, fields count and email messages in "Administer" -> "Store administration" -> "Configuration" -> "Sponsorship"
5. Enable the "Sponsorship" block under "Site building" -> "Blocks"

Reports:
------------
For now, Übercart Sponsorship gives you really simple feedbacks.
To see go to : "Administer" -> "Store administration" -> "Customers" -> "Sponsorship"
You also download this data as CSV from link which should be self explanatory.

TODO:
------------
Let user know how discount he/she gets from sponsorship on his account page.
Integrate directly with Discount module to let you choose from an admin choosen uc_discount rule?
Integrate with user_points?